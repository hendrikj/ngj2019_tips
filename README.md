## Grundlagen

Um die Grundlagen von Unity zu lernen gibt es viele Ressourcen Online, wenn man keine oder nur wenig Erfahrung mit Unity sind die folgenden Guides ein guter Anfang:

* [Introduction to Unity Part 1](https://www.raywenderlich.com/7514-introduction-to-unity-getting-started-part-1-2)
* [Introduction to Unity Part 2](https://www.raywenderlich.com/9175-introduction-to-unity-getting-started-part-2-2)


* [Introduction to Unity Scripting Part 1](https://www.raywenderlich.com/4180726-introduction-to-unity-scripting-part-1)
* [Introduction to Unity Scripting Part 2](https://www.raywenderlich.com/4180875-introduction-to-unity-scripting-part-2)

## Weitere Tipps

### Szenen

Um schwierige Mergekonflikte zu vermeiden ist es zu empfehlen, dass nicht alle Leute in der selben Unity Szene arbeiten. Dies führt zu unnötigen Mergekonflikten, die von Hand kaum zu lösen sind. Es sollte jeder in einer eigenen Szene arbeiten um dort neue Features zu entwickeln. Ausnahme: Wenn man in der selben Szene arbeitet, nur eine Änderungen an der Szene vornimmt, und die Anderen beispielsweise nur am Code arbeiten.

### .gitignore
Wenn man zusammen an einem Unity-Projekt arbeitet, ist es eine gute Idee mit einer `.gitignore` Datei zu verhindern, dass manche Dateien in das git Repository eingecheckt werden.

Sinnvolle einträge für eine `.gitignore` sind z.B.:
```
**/.vs/**
**/Library/**
**/obj/**
**/Temp/**
**/Logs/**

*.sln
*.csproj
*.log
```

In diesem Repository ist eine .gitignore mit sinnvollen Einträgen für Unity-Projekte enthalten, die man als Grundlage für das eigene Projekt Nutzen kann.

### Update oder FixedUpdate ?

Der Untschied zwischen der `Update` und der `FixedUpdate` ist, dass die Update Methode für jeden einzelnen Frame aufgerufen wird. Dies ist nützlich um dynamische UI-Elemente zu bewegen, damit sie immer im richtigen Verhältnis zur Kameraposition gerendert werden können.

`FixedUpdate` wird hingegen immer dann aufgerufen, wenn Unitys Physics-Engine Positionen und Kollisionen berechnet. Dies passiert nicht bei jedem Frame sondern deutlich seltener. Viel Spiellogisches Verhalten dass z.B. von Kollisionen und Positionen abhängig ist, ist daher besser in der FixedUpdate Methode aufgehoben, wodurch es nur dann ausgeführt wird, wenn es nötig ist und dadurch die Performance nicht unnötig verringert.

### Scriptable Objects

Scriptable Objects sind persistente Objekte die man, wie zb neue Gameobjekte für seine Szene über den Unity Inspector anlegen kann.


#### Beispiel
Von einer Klasse wie dieser können über den Unity Inspektor persistente Instanzen angelegt werden, die dann z.B. in Skripten als Referenz mitgegeben werden können.

```
[CreateAssetMenu(fileName = "New Enemy Data", menuName = "Enemy/Enemy Data", order = 54)]
public class EnemyData : ScriptableObject
{
    public int hp;
    public float speed;
}
```
#### Ressourcen
* [Getting started withScriptable Objects](https://www.raywenderlich.com/2826197-scriptableobject-tutorial-getting-started)
* [Unit Scirptable Object API](https://docs.unity3d.com/Manual/class-ScriptableObject.html)

### C# Event Delegates

#### Emitter
```
# deklarieren 
public delegate void OnCraftingStarted(CStation station, Recipe recipe);
public event OnCraftingStarted OnCraftingStartedEvent;

# Event aufrufen
OnCraftingStartedEvent?.Invoke(this, recipe);
```
#### Consumer

```
# die Funktion die aufgerufen werden soll muss die gleiche Signatur wie der deklarierte delegate haben
private void Station_OnCraftingStarted(CStation station, Recipe recipe)

# für das Event anmelden
station.OnCraftingStartedEvent += Station_OnCraftingStarted;

# man muss sich auch immer wieder abmelden (oftmals ist `OnDisable` ein guter Ort, da die Funktion immer aufgerufen wird wenn das Objekt zerstört wird)
# always unsubscribe from event at some point (often OnDisable is a good place since it gets called when the object gets destroyed)
station.OnCraftingStartedEvent -= Station_OnCraftingStarted;
```


